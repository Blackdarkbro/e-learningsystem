﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using CourseSavchik.ViewModels.TeacherViewModels;
using CourseSavchik.Views.TeacherViews;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using CourseSavchik.ViewModels;
using System.IO;
using CourseSavchik.Views.StudentViews;
using CourseSavchik.ViewModels.StudentViewModels;

namespace CourseSavchik.Views.StudentViews
{
    public class TestingViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public TestingViewModel(IMainWindowsCodeBehind codeBehind, Student student, string Theme)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;
            this.student = student;
            this.theme = Theme;

            if (TestsRepository.GetRandomTasksByTheme(Theme, 10).Count > 0)
            {
                List<Models.Task> tasks = TestsRepository.GetRandomTasksByTheme(Theme, 10);


                taskViews = new ObservableCollection<TaskViewModel>(tasks.Select(p => new TaskViewModel(p)));

                currentTask = taskViews.First();
                answList = new List<bool>();


                ErrorVisibility = false;
                TaskVisibility = true;
                ErrorString = null;
            }
            else
            {
                ErrorString = "Нет найденных задач по данной теме";
                ErrorVisibility = true;
                TaskVisibility = false;
            }
        }
        private Student student;
        private string theme;
        private List<bool> answList;
        private int taskIndex = 0;
        private TaskViewModel currentTask;
        public ObservableCollection<TaskViewModel> taskViews;


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private string answer = "";
        public string Answer
        {
            get { return answer; }
            set
            {
                answer = value;
                OnPropertyChanged("Answer");
            }
        }

        public TaskViewModel CurrentTask
        {
            get { return currentTask; }
            set
            {
                currentTask = value;
                OnPropertyChanged("CurrentTask");
            }
        }
        private string errorString;
        public string ErrorString
        {
            get { return errorString; }
            set
            {
                errorString = value;
                OnPropertyChanged("ErrorString");
            }
        }

        private bool errorVisibility = true;
        public bool ErrorVisibility
        {
            get { return errorVisibility; }
            set
            {
                errorVisibility = value;
                OnPropertyChanged("ErrorVisibility");
            }
        }
        private bool taskVisibility = false;
        public bool TaskVisibility
        {
            get { return taskVisibility; }
            set
            {
                taskVisibility = value;
                OnPropertyChanged("TaskVisibility");
            }
        }

        private RelayCommand backCommand;
        public RelayCommand BackCommand
        {
            get
            {
                return backCommand ??
                  (backCommand = new RelayCommand(obj =>
                  {
                      MainWindow window = App.Current.MainWindow as MainWindow;
                      StudentMainView view = new StudentMainView()
                      {
                          DataContext = new StudentMainViewModel(window, student)
                      };
                      window.OutputView.Content = view;
                  }));
            }
        }
        private RelayCommand answerCommand;
        public RelayCommand AnswerCommand
        {
            get
            {
                return answerCommand ??
                    (answerCommand = new RelayCommand(obj =>
                    {

                        if (answList.Count <= taskViews.Count)
                        {
                            string taskAnsw = currentTask.Answer.ToString();
                            string userAnsw = Answer.ToString();
                            if (userAnsw.Equals(taskAnsw))
                                answList.Add(true);
                            else
                                answList.Add(false);

                            if (answList.Count == taskViews.Count)
                                LoadNoteScreen(answList);

                            if (answList.Count + 1 <= taskViews.Count)
                            {
                                CurrentTask = taskViews.ElementAt(taskIndex + 1);
                                taskIndex++;
                            }
                            
                        }
                        else
                            LoadNoteScreen(answList);
                    }));
            }
        }
        private void LoadNoteScreen(List<bool> answers)
        {
            MainWindow window = App.Current.MainWindow as MainWindow;
            NoteView view = new NoteView()
            {
                DataContext = new NoteViewModel(window, Examiner.GetNote(answList), student, theme)
            };
            window.OutputView.Content = view;
        }
    }
}


