﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using CourseSavchik.ViewModels.TeacherViewModels;
using CourseSavchik.Views.TeacherViews;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using CourseSavchik.ViewModels;
using System.IO;
using CourseSavchik.Views.StudentViews;
using CourseSavchik.ViewModels.StudentViewModels;

namespace CourseSavchik.ViewModels.StudentViewModels
{
    public class NoteViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public NoteViewModel(IMainWindowsCodeBehind codeBehind, int note, Student student, string Theme)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;

            this.note = note;
            this.student = student;
            TestsRepository.AddTestNote(TestsRepository.GetTest(Theme), student, this.note);
        }
        private Student student;
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private int note;
        public int Note
        {
            get { return note; }
            set
            {
                note = value;
                OnPropertyChanged("Note");
            }
        }
        private RelayCommand backCommand;
        public RelayCommand BackCommand
        {
            get
            {
                return backCommand ??
                    (backCommand = new RelayCommand(obj =>
                    {
                        MainWindow mainWindow = App.Current.MainWindow as MainWindow;
                        StudentMainView view = new StudentMainView()
                        {
                            DataContext = new StudentMainViewModel(mainWindow, student)
                        };
                        mainWindow.OutputView.Content = view;
                    }
                    ));
            }
        }
    }
}
