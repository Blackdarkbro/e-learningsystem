﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;
using CourseSavchik.Views.StudentView;
using CourseSavchik.Views.StudentView.Theoretical;

namespace CourseSavchik.ViewModels.StudentViewModels
{
    public class TheorieViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public TheorieViewModel(IMainWindowsCodeBehind codeBehind)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;

            ButtonsVisibility = true;
            PagesVisibility = false;
            
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        private bool buttonsVisibility;
        public bool ButtonsVisibility
        {
            get { return buttonsVisibility; }
            set
            {
                buttonsVisibility = value;
                OnPropertyChanged("ButtonsVisibility");
            }
        }
        private bool pagesVisibility;
        public bool PagesVisibility
        {
            get { return pagesVisibility; }
            set
            {
                pagesVisibility = value;
                OnPropertyChanged("PagesVisibility");
            }
        }



        private RelayCommand searchCommand;
        public RelayCommand SearchCommand
        {
            get
            {
                return searchCommand ??
                    (searchCommand = new RelayCommand(obj =>
                    {
                        PagesVisibility = true;
                        ButtonsVisibility = false;

                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        TheorieView theorieView = mainView.OutputView.Content as TheorieView;

                        SearchAlg view = new SearchAlg();
                        theorieView.OutputView.Content = view;

                    }));
            }
        }

        private RelayCommand listCommand;
        public RelayCommand ListCommand
        {
            get
            {
                return listCommand ??
                    (listCommand = new RelayCommand(obj =>
                    {
                        PagesVisibility = true;
                        ButtonsVisibility = false;
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        TheorieView theorieView = mainView.OutputView.Content as TheorieView;

                        list view = new list();
                        theorieView.OutputView.Content = view;

                    }));
            }
        }

        private RelayCommand graphCommand;
        public RelayCommand GraphCommand
        {
            get
            {
                return graphCommand ??
                    (graphCommand = new RelayCommand(obj =>
                    {
                        PagesVisibility = true;
                        ButtonsVisibility = false;
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        TheorieView theorieView = mainView.OutputView.Content as TheorieView;

                        Graph view = new Graph();
                        theorieView.OutputView.Content = view;

                    }));
            }
        }

        private RelayCommand sortCommand;
        public RelayCommand SortCommand
        {
            get
            {
                return sortCommand ??
                    (sortCommand = new RelayCommand(obj =>
                    {
                        PagesVisibility = true;
                        ButtonsVisibility = false;
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        TheorieView theorieView = mainView.OutputView.Content as TheorieView;

                        Sort view = new Sort();
                        theorieView.OutputView.Content = view;

                    }));
            }
        }

        private RelayCommand stackCommand;
        public RelayCommand StackCommand
        {
            get
            {
                return stackCommand ??
                    (stackCommand = new RelayCommand(obj =>
                    {
                        PagesVisibility = true;
                        ButtonsVisibility = false;
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        TheorieView theorieView = mainView.OutputView.Content as TheorieView;

                        StackAndQueue view = new StackAndQueue();
                        theorieView.OutputView.Content = view;

                    }));
            }
        }

        private RelayCommand treeCommand;
        public RelayCommand TreeCommand
        {
            get
            {
                return treeCommand ??
                    (treeCommand = new RelayCommand(obj =>
                    {

                        PagesVisibility = true;
                        ButtonsVisibility = false;
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        TheorieView theorieView = mainView.OutputView.Content as TheorieView;

                        Tree view = new Tree();
                        theorieView.OutputView.Content = view;
                        
                    }));
            }
        }
    }
}
