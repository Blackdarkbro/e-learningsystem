﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using CourseSavchik.ViewModels.TeacherViewModels;
using CourseSavchik.Views.TeacherViews;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;
using CourseSavchik.Views.StudentViews;
using CourseSavchik.ViewModels.StudentViewModels;

namespace CourseSavchik.ViewModels
{
    public class StudentMainViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public StudentMainViewModel(IMainWindowsCodeBehind codeBehind, Student student)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;
            this.student = student;
        }
        private Student student;
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public string Login
        {
            get { return student.Login; }
        }
        private RelayCommand userInfoCommand;
        public RelayCommand UserInfoCommand
        {
            get
            {
                return userInfoCommand ??
                    (userInfoCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        UserInfoView view = new UserInfoView()
                        {
                            DataContext = new UserInfoViewModel(window, student)
                        };
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        mainView.OutputView.Content = view;
                    }));
            }
        }

        private RelayCommand exitCommand;
        public RelayCommand ExitCommand
        {
            get
            {
                return exitCommand ??
                    (exitCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        LoginView view = new LoginView()
                        {
                            DataContext = new LoginViewModel(window)
                        };
                        student = null;
                        window.OutputView.Content = view;
                    }));
            }
        }

        private RelayCommand theorieCommand;
        public RelayCommand TheorieCommand
        {
            get
            {
                return theorieCommand ??
                    (theorieCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        TheorieView view = new TheorieView()
                        {
                            DataContext = new TheorieViewModel(window)
                        };
                        mainView.OutputView.Content = view;
                    }));
            }
        }

        private RelayCommand testsCommand;
        public RelayCommand TestsCommand
        {
            get
            {
                return testsCommand ??
                    (testsCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StudentMainView mainView = window.OutputView.Content as StudentMainView;
                        ChooseTestView view = new ChooseTestView()
                        {
                            DataContext = new ChooseTestViewModel(window, student)
                        };
                        mainView.OutputView.Content = view;
                    }));
            }
        }
    }
}
