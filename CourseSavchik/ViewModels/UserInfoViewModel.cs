﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels
{
    public class UserInfoViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public UserInfoViewModel(IMainWindowsCodeBehind codeBehind, Teacher teacher)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            StudentVisibility = false;
            NoTestsErrorVisibility = false;
            _MainCodeBehind = codeBehind;
            this.Teacher = teacher;
            Password = Teacher.Password;
        }
        public UserInfoViewModel(IMainWindowsCodeBehind codeBehind, Student student)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;
            this.Student = student;
            Password = student.Password;
            if (TestsRepository.GetStudentsToTests(student).Count() > 0)
            {
                NoTestsErrorVisibility = false;
                StudentVisibility = true;
                tests = new ObservableCollection<TestsViewModel>(TestsRepository.GetUserFinishedTests(student).Select(p => new TestsViewModel(p, student)));
            }
            else
            {
                NoTestsErrorVisibility = true;
                StudentVisibility = false;
            }
        }
        private Teacher teacher;
        public Teacher Teacher
        {
            get { return teacher; }
            set
            {
                teacher = value;
                OnPropertyChanged("Teacher");
            }
        }

        private Student student;
        public Student Student
        {
            get { return student; }
            set
            {
                student = value;
                OnPropertyChanged("Student");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string Login
        {
            get
            {
                if (teacher == null)
                    return student.Login;
                if (student == null)
                    return teacher.Login;
                return null;
            }
        }
        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                OnPropertyChanged("Password");
            }
        }


        private bool studentVisibility;
        public bool StudentVisibility
        {
            get { return studentVisibility; }
            set
            {
                studentVisibility = value;
                OnPropertyChanged("StudentVsibility");
            }
        }

        private string newPassword;
        public string NewPassword
        {
            get { return newPassword; }
            set
            {
                newPassword = value;
                OnPropertyChanged("NewPassword");
            }
        }

        private RelayCommand changeCommand;
        public RelayCommand ChangeCommand
        {
            get
            {
                return changeCommand ??
                    (changeCommand = new RelayCommand(obj =>
                    {
                        if (NewPassword != null)
                        {
                            if (student != null)
                            {
                                Student tmp = student;
                                tmp.Password = NewPassword;
                                StudRepository.UpdateStudent(student.Login, tmp);
                                Student = StudRepository.GetStudent(student.Login);
                                Password = Student.Password;
                            }
                            if (teacher != null)
                            {
                                Teacher tmp = teacher;
                                tmp.Password = NewPassword;
                                StudRepository.UpdateTeacher(teacher.Login, tmp);
                                Teacher = StudRepository.GetTeacher(teacher.Login);
                                Password = Teacher.Password;
                            }
                        }
                    }));
            }
        }

        private ObservableCollection<TestsViewModel> tests;
        public ObservableCollection<TestsViewModel> Tests
        {
            get { return tests; }
            set
            {
                tests = value;
                OnPropertyChanged("Tests");
            }
        }

        private bool noTestsErrorVisibility;
        public bool NoTestsErrorVisibility
        {
            get { return noTestsErrorVisibility; }
            set
            {
                noTestsErrorVisibility = value;
                OnPropertyChanged("NoTestsErrorVisibility");
            }
        }
    }
}
