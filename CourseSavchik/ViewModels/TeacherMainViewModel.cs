﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using CourseSavchik.ViewModels.TeacherViewModels;
using CourseSavchik.Views.TeacherViews;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels
{
    public class TeacherMainViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public TeacherMainViewModel(IMainWindowsCodeBehind codeBehind, Teacher teacher)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;
            this.teacher = teacher;
        }
        private Teacher teacher;
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private RelayCommand userInfoCommand;
        public RelayCommand UserInfoCommand
        {
            get
            {
                return userInfoCommand ??
                    (userInfoCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        UserInfoView view = new UserInfoView()
                        {
                            DataContext = new UserInfoViewModel(window, teacher)
                        };
                        TeacherMainView mainView = window.OutputView.Content as TeacherMainView;
                        mainView.OutputView.Content = view;
                    }));
            }
        }

        private RelayCommand exitCommand;
        public RelayCommand ExitCommand
        {
            get
            {
                return exitCommand ??
                    (exitCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        LoginView view = new LoginView()
                        {
                            DataContext = new LoginViewModel(window)
                        };
                        teacher = null;
                        window.OutputView.Content = view;
                    }));
            }
        }

        private RelayCommand addTaskCommand;
        public RelayCommand AddTaskCommand
        {
            get
            {
                return addTaskCommand ??
                    (addTaskCommand = new RelayCommand(obj => 
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        TeacherMainView mainView = window.OutputView.Content as TeacherMainView;
                        AddTaskView view = new AddTaskView()
                        {
                            DataContext = new AddTaskViewModel(window)
                        };
                        mainView.OutputView.Content = view;
                    }));
            }
        }

        private RelayCommand removeTaskCommand;
        public RelayCommand RemoveTaskCommand
        {
            get
            {
                return removeTaskCommand ??
                    (removeTaskCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        RemoveTaskView view = new RemoveTaskView()
                        {
                            DataContext = new RemoveTaskViewModel(window)
                        };
                        TeacherMainView mainView = window.OutputView.Content as TeacherMainView;
                        mainView.OutputView.Content = view;
                    }));
            }
        }

        private RelayCommand addAccountCommand;
        public RelayCommand AddAccountCommand
        {
            get
            {
                return addAccountCommand ??
                    (addAccountCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        AddAccountView view = new AddAccountView()
                        {
                            DataContext = new AddAccountViewModel(window)
                        };
                        TeacherMainView mainView = window.OutputView.Content as TeacherMainView;
                        mainView.OutputView.Content = view;
                    }));
            }
        }

        private RelayCommand removeAccountCommand;
        public RelayCommand RemoveAccountCommand
        {
            get
            {
                return removeAccountCommand ??
                    (removeAccountCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        RemoveAccountView view = new RemoveAccountView()
                        {
                            DataContext = new RemoveAccountViewModel(window)
                        };
                        TeacherMainView mainView = window.OutputView.Content as TeacherMainView;
                        mainView.OutputView.Content = view;
                    }));
            }
        }
        private RelayCommand statsCommand;
        public RelayCommand StatsCommand
        {
            get
            {
                return statsCommand ??
                    (statsCommand = new RelayCommand(obj =>
                    {
                        MainWindow window = App.Current.MainWindow as MainWindow;
                        StatsView view = new StatsView()
                        {
                            DataContext = new StatsViewModel(window)
                        };
                        TeacherMainView mainView = window.OutputView.Content as TeacherMainView;
                        mainView.OutputView.Content = view;
                    }));
            }
        }

        public string Login
        {
            get { return teacher.Login; }
        }
    }
}
