﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels
{
    public class TaskViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public TaskViewModel(Task task)
        {
            this.task = task;
        }
        public Task task;

        public string Name
        {
            get { return task.Name; }
        }
        public string Answer
        {
            get { return task.Answer; }
        }
        public string Theme
        {
            get { return TestsRepository.GetTheme(task).Name; }
        }
        public string Text
        {
            get { return task.Text; }
        }
        public BitmapImage Image
        {
            get
            {
                if (task.Image != null)
                {
                    return ImageHandler.ToImage(task.Image);
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
