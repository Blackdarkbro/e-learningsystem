﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels.TeacherViewModels
{
    public class RemoveTaskViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public RemoveTaskViewModel(IMainWindowsCodeBehind codeBehind)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;
            if(TestsRepository.GetTasks().Count > 0)
            {
                NormalVisibility = true;
                ErrorVisiblity = false;
                Tasks = new ObservableCollection<TaskViewModel>(TestsRepository.GetTasks().Select(p => new TaskViewModel(p)));
            }
            else
            {
                NormalVisibility = false;
                ErrorVisiblity = true;
                ErrorString = "Нет созданных задач";
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private ObservableCollection<TaskViewModel> tasks;
        public ObservableCollection<TaskViewModel> Tasks
        {
            get { return tasks; }
            set
            {
                tasks = value;
                OnPropertyChanged("Tasks");
            }
        }

        private TaskViewModel selectedTask;
        public TaskViewModel SelectedTask
        {
            get { return selectedTask; }
            set
            {
                selectedTask = value;
                OnPropertyChanged("SelectedTask");
            }
        }

        private bool errorVisibility;
        public bool ErrorVisiblity
        {
            get { return errorVisibility; }
            set
            {
                errorVisibility = value;
                OnPropertyChanged("ErrorVisibility");
            }
        }

        private bool normalVisibility;
        public bool NormalVisibility
        {
            get { return normalVisibility; }
            set
            {
                normalVisibility = value;
                OnPropertyChanged("NormalVisibility");
            }
        }

        private RelayCommand deleteCommand;
        public RelayCommand DeleteCommand
        {
            get
            {
                return deleteCommand ??
                    (deleteCommand = new RelayCommand(obj =>
                    {
                        if (SelectedTask != null)
                        {
                            TestsRepository.RemoveTask(SelectedTask.task);
                            if (TestsRepository.GetTasks().Count > 0)
                            {
                                NormalVisibility = true;
                                ErrorVisiblity = false;
                                Tasks = new ObservableCollection<TaskViewModel>(TestsRepository.GetTasks().Select(p => new TaskViewModel(p)));
                            }
                            else
                            {
                                NormalVisibility = false;
                                ErrorVisiblity = true;
                                ErrorString = "Нет созданных задач";
                            }
                        }
                        else
                        {
                            ErrorString = "Задача не найдена";
                        }
                    }));
            }
        }
        private string errorString;
        public string ErrorString
        {
            get { return errorString; }
            set
            {
                errorString = value;
                OnPropertyChanged("ErrorString");
            }
        }

        private bool ValidateString(string String)
        {
            if (String != null && String != "")
                if (Regex.IsMatch(String, "^[а-яёa-z]{1}.+", RegexOptions.IgnoreCase))
                    return true;

            return false;
        }
        private bool ValidateNumber(string Number)
        {
            if (Number != null && Number != "")
                if (Regex.IsMatch(Number, "^\\d+$"))
                    return true;
            return false;
        }
    }
}
