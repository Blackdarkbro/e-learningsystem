﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels.TeacherViewModels
{
    public class StatsViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public StatsViewModel(IMainWindowsCodeBehind codeBehind)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;
        
            if(StudRepository.GetStudents().Count > 0)
            {
                AllVisibility = true;
                NoStudentsErrorVisibility = false;
                Students = new ObservableCollection<string>(StudRepository.GetStudents().Select(p => p.Login));
            }
            else
            {
                AllVisibility = false;
                NoStudentsErrorVisibility = true;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<string> students;
        public ObservableCollection<string> Students
        {
            get { return students; }
            set
            {
                students = value;
                OnPropertyChanged("Students");
            }
        }

        private string selectedStudent;
        public string SelectedStudent
        {
            get { return selectedStudent; }
            set
            {
                selectedStudent = value;
                OnPropertyChanged("SelectedValue");
                SelectionChanged();
            }
        }

        private bool allVisibility;
        public bool AllVisibility
        {
            get { return allVisibility; }
            set
            {
                allVisibility = value;
                OnPropertyChanged("AllVisibility");
            }
        }
        private bool tasksVisibility;
        public bool TasksVisibility
        {
            get { return tasksVisibility; }
            set
            {
                tasksVisibility = value;
                OnPropertyChanged("TasksVisibility");
            }
        }
        private bool noStudentsErrorVisibility;
        public bool NoStudentsErrorVisibility
        {
            get { return noStudentsErrorVisibility; }
            set
            {
                noStudentsErrorVisibility = value;
                OnPropertyChanged("NoStudentsErrorVisibility");
            }
        }
        private bool noTestsVisibility;
        public bool NoTestsVisibility
        {
            get { return noTestsVisibility; }
            set
            {
                noTestsVisibility = value;
                OnPropertyChanged("NoTestsVisibility");
            }
        }


        private ObservableCollection<TestsViewModel> tests;
        public ObservableCollection<TestsViewModel> Tests
        {
            get { return tests; }
            set
            {
                tests = value;
                OnPropertyChanged("Tests");
            }
        }

        public void SelectionChanged()
        {
            if (SelectedStudent != null)
            {
                if (TestsRepository.GetStudentsToTests(StudRepository.GetStudent(SelectedStudent)).Count() > 0)
                {
                    TasksVisibility = true;
                    NoTestsVisibility = false;

                    Tests = new ObservableCollection<TestsViewModel>(TestsRepository.GetUserFinishedTests(StudRepository.GetStudent(SelectedStudent)).Select(p => new TestsViewModel(p, StudRepository.GetStudent(SelectedStudent))));
                }
                else
                {
                    TasksVisibility = false;
                    NoTestsVisibility = true;
                }
            }
        }
        
             
    }
}
