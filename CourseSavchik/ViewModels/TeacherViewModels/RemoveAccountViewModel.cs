﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels.TeacherViewModels
{
    public class RemoveAccountViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public RemoveAccountViewModel(IMainWindowsCodeBehind codeBehind)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;

            if (StudRepository.GetStudents().Count > 0)
            {
                DeleteErrorVisibility = false;
                DeleteVisibility = true;
                Accounts = new ObservableCollection<StudentViewModel>(StudRepository.GetStudents().Select(p => new StudentViewModel(p)));
            }
            else
            {
                DeleteErrorVisibility = true;
                DeleteVisibility = false;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private ObservableCollection<StudentViewModel> accounts;
        public ObservableCollection<StudentViewModel> Accounts
        {
            get { return accounts; }
            set
            {
                accounts = value;
                OnPropertyChanged("Accounts");
            }
        }

        private StudentViewModel accountToDelete;
        public StudentViewModel AccountToDelete
        {
            get { return accountToDelete; }
            set
            {
                accountToDelete = value;
                OnPropertyChanged("AccountToDelete");
            }
        }

        private bool deleteVisibility = true;
        public bool DeleteVisibility
        {
            get { return deleteVisibility; }
            set
            {
                deleteVisibility = value;
                OnPropertyChanged("DeleteVisibility");
            }
        }
        private bool deleteErrorVisibility = false;
        public bool DeleteErrorVisibility
        {
            get { return deleteErrorVisibility; }
            set
            {
                deleteErrorVisibility = value;
                OnPropertyChanged("DeleteErrorVisibility");
            }
        }

        private bool ValidateString(string String)
        {
            if (String != null && String != "")
                if (Regex.IsMatch(String, "^[а-яёa-z]{1}.+", RegexOptions.IgnoreCase))
                    return true;

            return false;
        }
        private bool ValidateNumber(string Number)
        {
            if (Number != null && Number != "")
                if (Regex.IsMatch(Number, "^\\d+$"))
                    return true;
            return false;
        }


        private RelayCommand deleteCommand;
        public RelayCommand DeleteCommand
        {
            get
            {
                return deleteCommand ??
                    (deleteCommand = new RelayCommand(obj =>
                    {
                        if (AccountToDelete != null)
                        {
                            StudRepository.RemoveStudent(AccountToDelete.student);
                            if (StudRepository.GetStudents().Count > 0)
                            {
                                DeleteErrorVisibility = false;
                                DeleteVisibility = true;
                                Accounts = new ObservableCollection<StudentViewModel>(StudRepository.GetStudents().Select(p => new StudentViewModel(p)));
                            }
                            else
                            {
                                DeleteErrorVisibility = true;
                                DeleteVisibility = false;
                            }
                        }
                    }));
            }
        }
    }
}
