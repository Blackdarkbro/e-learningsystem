﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels.TeacherViewModels
{
    public class AddTaskViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public AddTaskViewModel(IMainWindowsCodeBehind codeBehind)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;

            Themes = new ObservableCollection<string>(TestsRepository.GetThemes().Select(p => p.Name));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string errorString = "";
        public string ErrorString
        {
            get { return errorString; }
            set
            {
                errorString = value;
                OnPropertyChanged("ErrorString");
            }
        }
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        private string text;
        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }
        private byte[] image;

        private ObservableCollection<string> themes;
        public ObservableCollection<string> Themes
        {
            get { return themes; }
            set
            {
                themes = value;
                OnPropertyChanged("Themes");
            }
        }

        private string selectedTheme;
        public string SelectedTheme
        {
            get { return selectedTheme; }
            set
            {
                selectedTheme = value;
                OnPropertyChanged("SelectedTheme");
            }
        }

        private string answer;
        public string Answer
        {
            get { return answer; }
            set
            {
                answer = value;
                OnPropertyChanged("Answer");
            }
        }
        
        private bool ValidateString(string String)
        {
            if (String != null && String != "")
                if (Regex.IsMatch(String, "^[а-яёa-z]{1}.+", RegexOptions.IgnoreCase))
                    return true;

            return false;
        }
        private bool ValidateNumber(string Number)
        {
            if (Number != null && Number != "")
                if (Regex.IsMatch(Number, "^\\d+$"))
                    return true;
            return false;
        }
        private RelayCommand addTaskCommand;
        public RelayCommand AddTaskCommand
        {
            get
            {
                return addTaskCommand ??
                    (addTaskCommand = new RelayCommand(obj =>
                    {
                        
                        if (ValidateString(name))
                        {
                            if (ValidateString(text))
                            {
                                if (SelectedTheme != null)
                                {
                                    if (Answer != null)
                                    {
                                        Task task = new Task();
                                        task.Name = Name;
                                        task.Answer = Answer;
                                        task.Image = image;
                                        task.ThemeID = TestsRepository.GetTheme(SelectedTheme).ID;
                                        task.Text = Text;

                                        TestsRepository.AddTask(task);

                                        Name = null;
                                        Answer = null;
                                        image = null;
                                        SelectedTheme = null;
                                        Text = null;

                                        ErrorString = "";
                                    }
                                    else
                                    {
                                        ErrorString = "Данные введены некорректно";
                                    }
                                }
                                else
                                {
                                    ErrorString = "Данные введены некорректно";
                                }

                            }
                            else
                            {
                                ErrorString = "Данные введены некорректно";
                            }
                        }
                        else
                        {
                            ErrorString = "Данные введены некорректно";
                        }

                    }));
            }
        }
        private RelayCommand addImageCommand;
        public RelayCommand AddImageCommand
        {
            get
            {
                return addImageCommand ??
                    (addImageCommand = new RelayCommand(obj =>
                    {
                        image = ImageHandler.ConvertBitmapSourceToByteArray(ImageHandler.GetImageFilePath());
                    }));
            }
        }
    }
}
