﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels.TeacherViewModels
{
    public class AddAccountViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public AddAccountViewModel(IMainWindowsCodeBehind codeBehind)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;

            Roles = new ObservableCollection<string>();

            Roles.Add("Преподаватель");
            Roles.Add("Студент");
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private bool ValidateString(string String)
        {
            if (String != null && String != "")
                if (Regex.IsMatch(String, "^[а-яёa-z]{1}.+", RegexOptions.IgnoreCase))
                    return true;

            return false;
        }
        private bool ValidateNumber(string Number)
        {
            if (Number != null && Number != "")
                if (Regex.IsMatch(Number, "^\\d+$"))
                    return true;
            return false;
        }
        private ObservableCollection<string> roles;
        public ObservableCollection<string> Roles
        {
            get { return roles; }
            set
            {
                roles = value;
                OnPropertyChanged("Roles");
            }
        }

        private string login;
        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                OnPropertyChanged("Login");
            }
        }
        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                OnPropertyChanged("Password");
            }
        }
        private string selectedRole;
        public string SelectedRole
        {
            get { return selectedRole; }
            set
            {
                selectedRole = value;
                OnPropertyChanged("SelectedRole");
            }
        }
        private string addError;
        public string AddError
        {
            get { return addError; }
            set
            {
                addError = value;
                OnPropertyChanged("AddError");
            }
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                    (addCommand = new RelayCommand(obj =>
                    {
                        bool flag = false;

                        if (SelectedRole != null)
                            if (ValidateString(Login))
                               if (Password != null && Password != "")
                               {
                                    if(SelectedRole == "Преподаватель")
                                    {
                                        Student student = new Student()
                                        {
                                            Login = login,
                                            Password = password
                                        };
                                        if (!StudRepository.ContainsStudent(student))
                                        {
                                            Teacher teacher = new Teacher()
                                            {
                                                Login = login,
                                                Password = password
                                            };
                                            if (!StudRepository.ContainsTeacher(teacher))
                                            {
                                                flag = true;
                                                StudRepository.AddTeacher(teacher);
                                            }
                                        }
                                    }
                                    if(SelectedRole == "Студент")
                                    {
                                        Teacher teacher = new Teacher()
                                        {
                                            Login = login,
                                            Password = password
                                        };
                                        if (!StudRepository.ContainsTeacher(teacher))
                                        {
                                            Student student = new Student()
                                            {
                                                Login = login,
                                                Password = password
                                            };
                                            if (!StudRepository.ContainsStudent(student))
                                            {
                                                flag = true;
                                                StudRepository.AddStudent(student);
                                            }
                                        }
                                    }
                               }
                        if (flag == false)
                            AddError = "Данные введены неверно";
                        else
                            AddError = null;
                    }));
            }
        }


    }
}
