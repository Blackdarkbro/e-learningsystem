﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        private IMainWindowsCodeBehind _MainCodeBehind;
        public LoginViewModel(IMainWindowsCodeBehind codeBehind)
        {
            if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

            _MainCodeBehind = codeBehind;

        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private RelayCommand exitCommand;
        public RelayCommand ExitCommand
        {
            get
            {
                return exitCommand ??
                    (exitCommand = new RelayCommand(obj =>
                    {
                        Application.Current.Shutdown();
                    }));
            }
        }
        private string login = null;
        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                OnPropertyChanged("Login");
            }
        }
        private bool errorVisibility = false;
        public bool ErrorVisibility
        {
            get { return errorVisibility; }
            set
            {
                errorVisibility = value;
                OnPropertyChanged("ErrorVisibility");
            }
        }
        private RelayCommand logInCommand;
        public RelayCommand LoginCommand
        {
            get
            {
                return logInCommand ??
                    (logInCommand = new RelayCommand(obj =>
                    {
                        var passwordBox = obj as PasswordBox;
                        string password = passwordBox.Password;
                        if (Login != null)
                            if (password != null && password != "")
                                if(StudRepository.ContainsStudent(new Student() {Login = Login, Password = password }))
                                {
                                    Student student = StudRepository.GetStudent(login);
                                    MainWindow window = App.Current.MainWindow as MainWindow;
                                    StudentMainView view = new StudentMainView()
                                    {
                                        DataContext = new StudentMainViewModel(window, student)
                                    };
                                    window.OutputView.Content = view;
                                }
                                else
                                {
                                    Teacher teacher = new Teacher()
                                    {
                                        Login = Login,
                                        Password = password
                                    };
                                    if (StudRepository.ContainsTeacher(teacher))
                                    {
                                        teacher = StudRepository.GetTeacher(Login);
                                        MainWindow window = App.Current.MainWindow as MainWindow;
                                        TeacherMainView view = new TeacherMainView()
                                        {
                                            DataContext = new TeacherMainViewModel(window, teacher)
                                        };
                                        window.OutputView.Content = view;
                                    }
                                }
                        ErrorVisibility = true;
                    }));
            }
        }

    }
}
