﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using CourseSavchik.Views;
using System.Collections.ObjectModel;
using CourseSavchik.Models;
using CourseSavchik.Commands;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.IO;

namespace CourseSavchik.ViewModels
{
    public class TestsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public TestsViewModel(Test test, Student student)
        {
            this.student = student;
            this.test = test;
            studentsToTest = TestsRepository.GetStudentsToTests(test,student).Single();
        }
        public Student student;
        public Test test;
        public StudentsToTest studentsToTest;

        public string Theme
        {
            get { return TestsRepository.GetTheme(test); }
        }
        public string Note
        {
            get { return studentsToTest.Note.ToString(); }
        }
    }
}
