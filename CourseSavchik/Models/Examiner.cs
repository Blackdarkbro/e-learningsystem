﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseSavchik.Models
{
    public static class Examiner
    {
        public static int GetNote(List<bool> answers)
        {
            int note = 0;
            
            int rightAnswersCount = answers.Where(p => (p == true)).ToList().Count;
            double procent = (double)rightAnswersCount / (double)answers.Count;

            if (procent >= 0 && procent < 0.1)
                note = 1;
            if (procent >= 0.1 && procent < 0.2)
                note = 2;
            if (procent >= 0.2 && procent < 0.3)
                note = 3;
            if (procent >= 0.3 && procent < 0.4)
                note = 4;
            if (procent >= 0.4 && procent < 0.5)
                note = 5;
            if (procent >= 0.5 && procent < 0.6)
                note = 6;
            if (procent >= 0.6 && procent < 0.7)
                note = 7;
            if (procent >= 0.7 && procent < 0.8)
                note = 8;
            if (procent >= 0.8 && procent < 1)
                note = 9;
            if (procent == 1)
                note = 10;
            return note;
        }
    }
}
