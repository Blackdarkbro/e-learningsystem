﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseSavchik.Models
{
    public static class StudRepository
    {
        static CourseSavchikDBEntities db;
        static StudRepository()
        {
            db = new CourseSavchikDBEntities();
        }


        public static bool ContainsTeacher(Teacher teacher)
        {
            if (db.Teachers.Where(p => p.Login == teacher.Login && p.Password == teacher.Password).Count() > 0)
                return true;
            return false;
        }
        public static void AddTeacher(Teacher teacher)
        {
            if(!(db.Teachers.Where(p => p.Login == teacher.Login).Count() > 0))
            {
                db.Teachers.Add(teacher);
                db.SaveChanges();
            }
        }
        public static void RemoveTeacher(Teacher teacher)
        {
            if(db.Teachers.Where(p => p.Login == teacher.Login).Count() > 0)
            {
                db.Teachers.Remove(teacher);
                db.SaveChanges();
            }
        }
        public static List<Teacher> GetTeachers()
        {
            return db.Teachers.ToList();
        }
        public static Teacher GetTeacher(string Login)
        {
            return db.Teachers.Where(p => p.Login == Login).Single();
        }
        public static void UpdateTeacher(string Login, Teacher teacher)
        {
            if (db.Teachers.Where(p => (p.Login == Login)).Count() > 0)
            {
                foreach (Teacher tTeacher in db.Teachers.Where(p => (p.Login == teacher.Login)))
                {
                    tTeacher.Login = teacher.Login;
                    tTeacher.Password = teacher.Password;
                    
                }
                db.SaveChanges();
            }
        }

        public static bool ContainsStudent(Student student)
        {
            if (db.Students.Where(p => p.Login == student.Login && p.Password == student.Password).Count() > 0)
                return true;
            return false;
        }
        public static void AddStudent(Student student)
        {
            if(db.Students.Where(p => p.Login == student.Login).Count() < 1)
            {
                db.Students.Add(student);
                db.SaveChanges();
            }
        }
        public static void RemoveStudent(Student student)
        {
            if(db.Students.Where(p => p.Login == student.Login).Count() > 0)
            {
                foreach(StudentsToTest studentsToTest in db.StudentsToTests.Where(p => p.StudentID == student.ID))
                {
                    db.StudentsToTests.Remove(studentsToTest);
                }
                db.Students.Remove(student);
                db.SaveChanges();
            }
        }
        public static List<Student> GetStudents()
        {
            return db.Students.ToList();
        }
        public static Student GetStudent(string Login)
        {
            return db.Students.Where(p => p.Login == Login).Single();
        }
        public static void UpdateStudent(string Login, Student student)
        {
            if (db.Students.Where(p => (p.Login == Login)).Count() > 0)
            {
                foreach (Student sStudent in db.Students.Where(p => (p.Login == student.Login)))
                {
                    sStudent.Login = student.Login;
                    sStudent.Password = student.Password;
                }
                db.SaveChanges();
            }
        }

        public static void UpdateTests(string Login, int TestID, int Note)
        {
            Student student = db.Students.Where(p => p.Login == Login).Single();
            if(db.StudentsToTests.Where(p => p.StudentID == student.ID && p.TestID == TestID).Count() < 1)
            {
                db.StudentsToTests.Add(new StudentsToTest() { StudentID = student.ID, TestID = TestID });
                db.SaveChanges();
            }
            else
            {
                foreach (StudentsToTest studentsToTest in db.StudentsToTests.Where(p => p.StudentID == student.ID && p.TestID == TestID))
                {
                    studentsToTest.Note = Note;
                }
                db.SaveChanges();
            }
        }
    }
}
