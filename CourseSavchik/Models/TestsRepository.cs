﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseSavchik.Models
{
    public static class TestsRepository
    {
        static CourseSavchikDBEntities db;
        static TestsRepository()
        {
            db = new CourseSavchikDBEntities();
        }
        public static void AddTask(Task task)
        {
            if (db.Tasks.Where(p => p.Name == task.Name).Count() < 1)
            {
                db.Tasks.Add(task);
                db.SaveChanges();
            }
        }


        public static List<StudentsToTest> GetStudentsToTests()
        {
            return db.StudentsToTests.ToList();
        }
        public static List<StudentsToTest> GetStudentsToTests(Student student)
        {
            return db.StudentsToTests.Where(p => p.StudentID == student.ID).ToList();
        }
        public static List<StudentsToTest> GetStudentsToTests(Test test, Student student)
        {
            return db.StudentsToTests.Where(p => p.TestID == test.ID && p.StudentID == student.ID).ToList();
        }

        public static Theme GetTheme(Task task)
        {
            return db.Themes.Where(p => p.ID == task.ThemeID).Single();
        }
        public static Theme GetTheme(string Theme)
        {
            return db.Themes.Where(p => p.Name == Theme).Single();
        }
        public static List<Theme> GetThemes()
        {
            return db.Themes.ToList();
        }
        public static List<Test> GetTests()
        {
            return db.Tests.ToList();
        }
        public static int GetTestNote(Test test, Student student)
        {
            return db.StudentsToTests.Where(p => p.StudentID == student.ID && p.TestID == test.ID).Single().Note;
        }
        public static void AddTestNote(Test test, Student student, int Note)
        {
            StudentsToTest studentsToTest = new StudentsToTest()
            {
                StudentID = student.ID,
                TestID = test.ID,
                Note = Note
            };
            if (db.StudentsToTests.Where(p => p.StudentID == studentsToTest.StudentID && p.TestID == studentsToTest.TestID).Count() > 0)
            {
                foreach (StudentsToTest toTest in db.StudentsToTests.Where(p => p.StudentID == studentsToTest.StudentID && p.TestID == studentsToTest.TestID))
                {
                    toTest.Note = studentsToTest.Note;
                }
                db.SaveChanges();
            }
            else
            {
                db.StudentsToTests.Add(studentsToTest);
                db.SaveChanges();
            }
        }
        public static Test GetTest(string Theme)
        {
            int themeID = GetTheme(Theme).ID;
            return db.Tests.Where(p => p.ThemeID == themeID).Single();
        }
        public static List<Test> GetUserFinishedTests(Student student)
        {
            List<StudentsToTest> studentsToTests = db.StudentsToTests.Where(p => p.StudentID == student.ID).ToList();
            List<Test> tests = new List<Test>();
            foreach (StudentsToTest studentsToTest in studentsToTests)
            {
                foreach (Test test in db.Tests)
                {
                    if (test.ID == studentsToTest.TestID)
                    {
                        tests.Add(test);
                    }
                }
            }
            return tests;
        }


        public static string GetTheme(Test test)
        {
            return db.Themes.Where(p => p.ID == test.ThemeID).Single().Name;
        }

        public static void RemoveTask(Task task)
        {
            db.Tasks.Remove(task);
            db.SaveChanges();
        }

        public static List<Task> GetTasks()
        {
            return db.Tasks.ToList();
        }

        public static List<Task> GetRandomTasksByTheme(string Theme, int Count)
        {
            int themeID = GetTheme(Theme).ID;
            List<Task> tasks = db.Tasks.Where(p => p.ThemeID == themeID).ToList();
            if (tasks.Count > Count)
            {
                Random random = new Random();
                List<Task> finalTasks = new List<Task>();
                for (int i = 0; i < Count; i++)
                {
                    int index = random.Next(1, tasks.Count) - 1;
                    finalTasks.Add(tasks[index]);
                    tasks.Remove(tasks[index]);
                }
                return finalTasks;
            }
            else
            {
                Random random = new Random();
                List<Task> finalTasks = new List<Task>();
                for (int i = 0; i <= tasks.Count; i++)
                {
                    int index = random.Next(1, tasks.Count) - 1;
                    finalTasks.Add(tasks[index]);
                    tasks.Remove(tasks[index]);
                }
                return finalTasks;
            }
        }


    }
}
