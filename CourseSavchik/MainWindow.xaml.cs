﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CourseSavchik.Models;
using CourseSavchik.ViewModels;
using CourseSavchik.Views;

namespace CourseSavchik
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    public interface IMainWindowsCodeBehind
    {
        void ShowMessage(string message);
    }
    public partial class MainWindow : Window, IMainWindowsCodeBehind
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LoginView view = new LoginView()
            {
                DataContext = new LoginViewModel(this)
            };
            this.OutputView.Content = view;
        }

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}
