﻿<UserControl x:Class="CourseSavchik.Views.StudentView.Theoretical.SearchAlg"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:CourseSavchik.Views.StudentView.Theoretical"
        mc:Ignorable="d"
        >
    <Grid Background="#d9d9d9">
        <FlowDocumentReader>
            <FlowDocument>
                <Paragraph TextAlignment="Center" FontFamily="Arial Black" FontWeight="Bold" FontSize="22" TextDecorations="{x:Null}">
                    <Run FontStretch="ExtraExpanded">Алгоритмы поиска</Run>
                </Paragraph>
                <Paragraph>
                    <Span FontWeight="Bold">Поиск</Span>
                    — обработка некоторого множества данных с целью выявления подмножества данных, соответствующего критериям поиска.
                    <LineBreak/>
                    Все алгоритмы поиска делятся на
                </Paragraph>
                <List MarkerStyle="Box">
                    <ListItem>
                        <Paragraph>поиск в неупорядоченном множестве данных;</Paragraph></ListItem>
                    <ListItem>
                        <Paragraph>поиск в упорядоченном множестве данных.</Paragraph></ListItem>
                </List>
                <Section>
                    <Paragraph FontWeight="Bold">Последовательный поиск</Paragraph>
                    <Paragraph>Начиная с первого элемента в списке, мы просто движемся от значения к значению, следуя внутреннему порядку последовательности, до тех пор, пока либо не найдём то, что ищем, либо не достигнем последнего элемента. Второй случай означает, что последовательность искомое не содержит.</Paragraph>
                    <BlockUIContainer>
                        <Image Source="/Images/seqsearch.png"/>
                    </BlockUIContainer>
                    <Paragraph>Как видно, это самый простой из алгоритмов поиска, и при больших объёмах данных поиск может занять довольно длительный временной интервал.</Paragraph>
                </Section>
                <Section>
                    <Paragraph FontWeight="Bold">Индексно-последовательный поиск</Paragraph>
                    <Paragraph>Для индексно-последовательного поиска в дополнение к отсортированной таблице заводится вспомогательная таблица, называемая
                        <Span FontWeight="Bold">индексной</Span> .</Paragraph>
                    <Paragraph>Каждый элемент индексной таблицы состоит из ключа и указателя на запись в основной таблице, соответствующей этому ключу. Элементы в индексной таблице, как элементы в основной таблице, должны быть отсортированы по этому ключу.</Paragraph>
                    <Paragraph>
                        Если индекс имеет размер, составляющий 1/8 от размера основной таблицы, то каждая восьмая запись основной таблицы будет представлена в индексной таблице.
                    </Paragraph>
                    <BlockUIContainer>
                        <Image Source="/Images/search1.png"/>
                    </BlockUIContainer>
                    <Paragraph>
                        Если размер основной таблицы —
                        <Span FontWeight="Bold">n</Span> , то размер индексной таблицы —
                        <Span FontWeight="Bold">ind_size = n/8</Span> .
                        <LineBreak></LineBreak>
                        Достоинство алгоритма индексно-последовательного поиска заключается в том, что сокращается время поиска, так как последовательный поиск первоначально ведется в индексной таблице, имеющей меньший размер, чем основная таблица. Когда найден правильный индекс, второй последовательный поиск выполняется по небольшой части записей основной таблицы.                        
                    </Paragraph>
                </Section>
                <Section>
                    <Paragraph FontWeight="Bold">Бинарный поиск</Paragraph>
                    <Paragraph Background="#FFB5F7F4" Padding="5">
                        Бинарный поиск производится в упорядоченном массиве.
                        <LineBreak/>
                        При бинарном поиске искомый ключ сравнивается с ключом среднего элемента в массиве. Если они равны, то поиск успешен. В противном случае поиск осуществляется аналогично в левой или правой частях массива.
Бинарный поиск также называют поиском методом
                        <Span FontWeight="Bold">деления отрезка пополам</Span> или
                        <Span FontWeight="Bold">дихотомии</Span> .
                    </Paragraph>
                    <Paragraph>
                        Количество шагов поиска определится как
                        <LineBreak/>
                        log2n↑,
                        <LineBreak/>
                        где
                        <Span FontWeight="Bold">n</Span> -количество элементов,
                        <LineBreak/>
                        ↑ — округление в большую сторону до ближайшего целого числа.
                        <LineBreak/>
                        На каждом шаге осуществляется поиск середины отрезка по формуле
                        <LineBreak/>
                        <Span FontWeight="Bold">mid = (left + right)/2</Span>
                        <LineBreak/>
                        Если искомый элемент равен элементу с индексом mid, поиск завершается.
В случае если искомый элемент меньше элемента с индексом mid, на место mid перемещается правая граница рассматриваемого отрезка, в противном случае — левая граница.
                    </Paragraph>
                    <Paragraph>
                        <Span FontWeight="Bold">Подготовка. </Span>
                        Перед началом поиска устанавливаем левую и правую границы массива:
                        <LineBreak/>
                        <Span FontWeight="Bold">left = 0, right = 19</Span>
                    </Paragraph>
                    <BlockUIContainer>
                        <Image Source="/Images/search3.png"/>
                    </BlockUIContainer>
                    <List>
                        <ListItem Margin="5">
                            <Paragraph>
                                <Span FontWeight="Bold">Шаг первый</Span>
                                Ищем индекс середины массива (округляем в меньшую сторону):
                                <LineBreak/>
                                <Span FontWeight="Bold">mid = (19+0)/2=9</Span>
                                <LineBreak/> Сравниваем значение по этому индексу с искомым:
                                <LineBreak></LineBreak><Span FontWeight="Bold">82 > 69</Span>
                                <LineBreak/> Сдвигаем левую границу:
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">left = mid = 9</Span>
                            </Paragraph>
                        </ListItem>
                        <ListItem Margin="5">
                            <Paragraph>
                                <Span FontWeight="Bold">Шаг второй</Span>
                                Ищем индекс середины массива (округляем в меньшую сторону):
                                <LineBreak/>
                                <Span FontWeight="Bold">mid = (9+19)/2=14</Span>
                                <LineBreak/> Сравниваем значение по этому индексу с искомым:
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">84 > 82</Span>
                                <LineBreak/> Сдвигаем левую границу:
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">right = mid = 14</Span>
                            </Paragraph>
                        </ListItem>
                        <ListItem Margin="5">
                            <Paragraph>
                                <Span FontWeight="Bold">Шаг третий</Span>
                                Ищем индекс середины массива (округляем в меньшую сторону):
                                <LineBreak/>
                                <Span FontWeight="Bold">mid = (9+14)/2=11</Span>
                                <LineBreak/> Сравниваем значение по этому индексу с искомым:
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">82 > 78</Span>
                                <LineBreak/> Сдвигаем левую границу:
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">left = mid = 11</Span>
                            </Paragraph>
                        </ListItem>
                        <ListItem Margin="5">
                            <Paragraph>
                                <Span FontWeight="Bold">Шаг четвёртый</Span>
                                Ищем индекс середины массива (округляем в меньшую сторону):
                                <LineBreak/>
                                <Span FontWeight="Bold">mid = (11+14)/2=12</Span>
                                <LineBreak/> Сравниваем значение по этому индексу с искомым:
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">82 > 80</Span>
                                <LineBreak/> Сдвигаем левую границу:
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">left = mid = 12</Span>
                            </Paragraph>
                        </ListItem>
                        <ListItem Margin="5">
                            <Paragraph>
                                <Span FontWeight="Bold">Шаг пятый</Span>
                                Ищем индекс середины массива (округляем в меньшую сторону):
                                <LineBreak/>
                                <Span FontWeight="Bold">mid = (12+14)/2=13</Span>
                                <LineBreak/> Сравниваем значение по этому индексу с искомым:
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">82 = 82</Span>
                                <LineBreak></LineBreak>
                                <Span FontWeight="Bold">Решение найдено</Span>
                            </Paragraph>
                        </ListItem>
                    </List>
                </Section>
            </FlowDocument>
        </FlowDocumentReader>
    </Grid>
</UserControl>
